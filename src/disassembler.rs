use super::mos6502::CpuBus;

pub fn disassemble(bus: &mut impl CpuBus, pc: u16) -> String {
    match bus.read_debugger(pc) {
        0x00 => String::from(format!("{:04x}  brk {}", pc,  immediate(bus, pc))),
        0x01 => String::from(format!("{:04x}  ora {}", pc,  indirect_x(bus, pc))),
        0x05 => String::from(format!("{:04x}  ora {}", pc,  zeropage(bus, pc))),
        0x06 => String::from(format!("{:04x}  asl {}", pc,  zeropage(bus, pc))),
        0x08 => String::from(format!("{:04x}  php {}", pc,  implied(bus, pc))),
        0x09 => String::from(format!("{:04x}  ora {}", pc,  immediate(bus, pc))),
        0x0a => String::from(format!("{:04x}  asl {}", pc,  implied(bus, pc))),
        0x0d => String::from(format!("{:04x}  ora {}", pc,  absolute(bus, pc))),
        0x0e => String::from(format!("{:04x}  asl {}", pc,  absolute(bus, pc))),
        0x10 => String::from(format!("{:04x}  bpl {}", pc,  branch(bus, pc))),
        0x11 => String::from(format!("{:04x}  ora {}", pc,  indirect_y(bus, pc))),
        0x15 => String::from(format!("{:04x}  ora {}", pc,  zeropage_x(bus, pc))),
        0x16 => String::from(format!("{:04x}  asl {}", pc,  zeropage_x(bus, pc))),
        0x18 => String::from(format!("{:04x}  clc {}", pc,  implied(bus, pc))),
        0x19 => String::from(format!("{:04x}  ora {}", pc,  absolute_y(bus, pc))),
        0x1d => String::from(format!("{:04x}  ora {}", pc,  absolute_x(bus, pc))),
        0x1e => String::from(format!("{:04x}  asl {}", pc,  absolute_x(bus, pc))),
        0x20 => String::from(format!("{:04x}  jsr {}", pc,  absolute(bus, pc))),
        0x21 => String::from(format!("{:04x}  and {}", pc,  indirect_x(bus, pc))),
        0x24 => String::from(format!("{:04x}  bit {}", pc,  zeropage(bus, pc))),
        0x25 => String::from(format!("{:04x}  and {}", pc,  zeropage(bus, pc))),
        0x26 => String::from(format!("{:04x}  rol {}", pc,  zeropage(bus, pc))),
        0x28 => String::from(format!("{:04x}  plp {}", pc,  implied(bus, pc))),
        0x29 => String::from(format!("{:04x}  and {}", pc,  immediate(bus, pc))),
        0x2a => String::from(format!("{:04x}  rol {}", pc,  implied(bus, pc))),
        0x2c => String::from(format!("{:04x}  bit {}", pc,  absolute(bus, pc))),
        0x2d => String::from(format!("{:04x}  and {}", pc,  absolute(bus, pc))),
        0x2e => String::from(format!("{:04x}  rol {}", pc,  absolute(bus, pc))),
        0x30 => String::from(format!("{:04x}  bmi {}", pc,  branch(bus, pc))),
        0x31 => String::from(format!("{:04x}  and {}", pc,  indirect_y(bus, pc))),
        0x35 => String::from(format!("{:04x}  and {}", pc,  zeropage_x(bus, pc))),
        0x36 => String::from(format!("{:04x}  rol {}", pc,  zeropage_x(bus, pc))),
        0x38 => String::from(format!("{:04x}  sec {}", pc,  implied(bus, pc))),
        0x39 => String::from(format!("{:04x}  and {}", pc,  absolute_y(bus, pc))),
        0x3d => String::from(format!("{:04x}  and {}", pc,  absolute_x(bus, pc))),
        0x3e => String::from(format!("{:04x}  rol {}", pc,  absolute_x(bus, pc))),
        0x40 => String::from(format!("{:04x}  rti {}", pc,  implied(bus, pc))),
        0x41 => String::from(format!("{:04x}  eor {}", pc,  indirect_x(bus, pc))),
        0x45 => String::from(format!("{:04x}  eor {}", pc,  zeropage(bus, pc))),
        0x46 => String::from(format!("{:04x}  lsr {}", pc,  zeropage(bus, pc))),
        0x48 => String::from(format!("{:04x}  pha {}", pc,  implied(bus, pc))),
        0x49 => String::from(format!("{:04x}  eor {}", pc,  immediate(bus, pc))),
        0x4a => String::from(format!("{:04x}  lsr {}", pc,  implied(bus, pc))),
        0x4c => String::from(format!("{:04x}  jmp {}", pc,  absolute(bus, pc))),
        0x4d => String::from(format!("{:04x}  eor {}", pc,  absolute(bus, pc))),
        0x4e => String::from(format!("{:04x}  lsr {}", pc,  absolute(bus, pc))),
        0x50 => String::from(format!("{:04x}  bvc {}", pc,  branch(bus, pc))),
        0x51 => String::from(format!("{:04x}  eor {}", pc,  indirect_y(bus, pc))),
        0x55 => String::from(format!("{:04x}  eor {}", pc,  zeropage_x(bus, pc))),
        0x56 => String::from(format!("{:04x}  lsr {}", pc,  zeropage_x(bus, pc))),
        0x58 => String::from(format!("{:04x}  cli {}", pc,  implied(bus, pc))),
        0x59 => String::from(format!("{:04x}  eor {}", pc,  absolute_y(bus, pc))),
        0x5a => String::from(format!("{:04x}  phy {}", pc,  implied(bus, pc))),
        0x5d => String::from(format!("{:04x}  eor {}", pc,  absolute_x(bus, pc))),
        0x5e => String::from(format!("{:04x}  lsr {}", pc,  absolute_x(bus, pc))),
        0x60 => String::from(format!("{:04x}  rts {}", pc,  implied(bus, pc))),
        0x61 => String::from(format!("{:04x}  adc {}", pc,  indirect_x(bus, pc))),
        0x64 => String::from(format!("{:04x}  stz {}", pc,  zeropage(bus, pc))),
        0x65 => String::from(format!("{:04x}  adc {}", pc,  zeropage(bus, pc))),
        0x66 => String::from(format!("{:04x}  ror {}", pc,  zeropage(bus, pc))),
        0x68 => String::from(format!("{:04x}  pla {}", pc,  implied(bus, pc))),
        0x69 => String::from(format!("{:04x}  adc {}", pc,  immediate(bus, pc))),
        0x6a => String::from(format!("{:04x}  ror {}", pc,  implied(bus, pc))),
        0x6c => String::from(format!("{:04x}  jmp {}", pc,  indirect(bus, pc))),
        0x6d => String::from(format!("{:04x}  adc {}", pc,  absolute(bus, pc))),
        0x6e => String::from(format!("{:04x}  ror {}", pc,  absolute(bus, pc))),
        0x70 => String::from(format!("{:04x}  bvs {}", pc,  branch(bus, pc))),
        0x71 => String::from(format!("{:04x}  adc {}", pc,  indirect_y(bus, pc))),
        0x74 => String::from(format!("{:04x}  stz {}", pc,  zeropage_x(bus, pc))),
        0x75 => String::from(format!("{:04x}  adc {}", pc,  zeropage_x(bus, pc))),
        0x76 => String::from(format!("{:04x}  ror {}", pc,  zeropage_x(bus, pc))),
        0x78 => String::from(format!("{:04x}  sei {}", pc,  implied(bus, pc))),
        0x79 => String::from(format!("{:04x}  adc {}", pc,  absolute_y(bus, pc))),
        0x7a => String::from(format!("{:04x}  ply {}", pc,  implied(bus, pc))),
        0x7d => String::from(format!("{:04x}  adc {}", pc,  absolute_x(bus, pc))),
        0x7e => String::from(format!("{:04x}  ror {}", pc,  absolute_x(bus, pc))),
        0x81 => String::from(format!("{:04x}  sta {}", pc,  indirect_x(bus, pc))),
        0x84 => String::from(format!("{:04x}  sty {}", pc,  zeropage(bus, pc))),
        0x85 => String::from(format!("{:04x}  sta {}", pc,  zeropage(bus, pc))),
        0x86 => String::from(format!("{:04x}  stx {}", pc,  zeropage(bus, pc))),
        0x88 => String::from(format!("{:04x}  dey {}", pc,  implied(bus, pc))),
        0x8a => String::from(format!("{:04x}  txa {}", pc,  implied(bus, pc))),
        0x8c => String::from(format!("{:04x}  sty {}", pc,  absolute(bus, pc))),
        0x8d => String::from(format!("{:04x}  sta {}", pc,  absolute(bus, pc))),
        0x8e => String::from(format!("{:04x}  stx {}", pc,  absolute(bus, pc))),
        0x90 => String::from(format!("{:04x}  bcc {}", pc,  branch(bus, pc))),
        0x91 => String::from(format!("{:04x}  sta {}", pc,  indirect_y(bus, pc))),
        0x94 => String::from(format!("{:04x}  sty {}", pc,  zeropage_x(bus, pc))),
        0x95 => String::from(format!("{:04x}  sta {}", pc,  zeropage_x(bus, pc))),
        0x96 => String::from(format!("{:04x}  stx {}", pc,  zeropage_y(bus, pc))),
        0x98 => String::from(format!("{:04x}  tya {}", pc,  implied(bus, pc))),
        0x99 => String::from(format!("{:04x}  sta {}", pc,  absolute_y(bus, pc))),
        0x9a => String::from(format!("{:04x}  txs {}", pc,  implied(bus, pc))),
        0x9c => String::from(format!("{:04x}  stz {}", pc,  absolute(bus, pc))),
        0x9d => String::from(format!("{:04x}  sta {}", pc,  absolute_x(bus, pc))),        
        0x9e => String::from(format!("{:04x}  stz {}", pc,  absolute_x(bus, pc))),
        0xa0 => String::from(format!("{:04x}  ldy {}", pc,  immediate(bus, pc))),
        0xa1 => String::from(format!("{:04x}  lda {}", pc,  indirect_x(bus, pc))),
        0xa2 => String::from(format!("{:04x}  ldx {}", pc,  immediate(bus, pc))),
        0xa4 => String::from(format!("{:04x}  ldy {}", pc,  zeropage(bus, pc))),
        0xa5 => String::from(format!("{:04x}  lda {}", pc,  zeropage(bus, pc))),
        0xa6 => String::from(format!("{:04x}  ldx {}", pc,  zeropage(bus, pc))),
        0xa8 => String::from(format!("{:04x}  tay {}", pc,  implied(bus, pc))),
        0xa9 => String::from(format!("{:04x}  lda {}", pc,  immediate(bus, pc))),
        0xaa => String::from(format!("{:04x}  tax {}", pc,  implied(bus, pc))),
        0xac => String::from(format!("{:04x}  ldy {}", pc,  absolute(bus, pc))),
        0xad => String::from(format!("{:04x}  lda {}", pc,  absolute(bus, pc))),
        0xae => String::from(format!("{:04x}  ldx {}", pc,  absolute(bus, pc))),
        0xb0 => String::from(format!("{:04x}  bcs {}", pc,  branch(bus, pc))),
        0xb1 => String::from(format!("{:04x}  lda {}", pc,  indirect_y(bus, pc))),
        0xb4 => String::from(format!("{:04x}  ldy {}", pc,  zeropage_x(bus, pc))),
        0xb5 => String::from(format!("{:04x}  lda {}", pc,  zeropage_x(bus, pc))),
        0xb6 => String::from(format!("{:04x}  ldx {}", pc,  zeropage_y(bus, pc))),
        0xb8 => String::from(format!("{:04x}  clv {}", pc,  implied(bus, pc))),
        0xb9 => String::from(format!("{:04x}  lda {}", pc,  absolute_y(bus, pc))),
        0xba => String::from(format!("{:04x}  tsx {}", pc,  implied(bus, pc))),
        0xbc => String::from(format!("{:04x}  ldy {}", pc,  absolute_x(bus, pc))),
        0xbd => String::from(format!("{:04x}  lda {}", pc,  absolute_x(bus, pc))),
        0xbe => String::from(format!("{:04x}  ldx {}", pc,  absolute_y(bus, pc))),
        0xc0 => String::from(format!("{:04x}  cpy {}", pc,  immediate(bus, pc))),
        0xc1 => String::from(format!("{:04x}  cmp {}", pc,  indirect_x(bus, pc))),
        0xc4 => String::from(format!("{:04x}  cpy {}", pc,  zeropage(bus, pc))),
        0xc5 => String::from(format!("{:04x}  cmp {}", pc,  zeropage(bus, pc))),
        0xc6 => String::from(format!("{:04x}  dec {}", pc,  zeropage(bus, pc))),
        0xc8 => String::from(format!("{:04x}  iny {}", pc,  implied(bus, pc))),
        0xc9 => String::from(format!("{:04x}  cmp {}", pc,  immediate(bus, pc))),
        0xca => String::from(format!("{:04x}  dex {}", pc,  implied(bus, pc))),
        0xcc => String::from(format!("{:04x}  cpy {}", pc,  absolute(bus, pc))),
        0xcd => String::from(format!("{:04x}  cmp {}", pc,  absolute(bus, pc))),
        0xce => String::from(format!("{:04x}  dec {}", pc,  absolute(bus, pc))),
        0xd0 => String::from(format!("{:04x}  bne {}", pc,  branch(bus, pc))),
        0xd1 => String::from(format!("{:04x}  cmp {}", pc,  indirect_y(bus, pc))),
        0xd5 => String::from(format!("{:04x}  cmp {}", pc,  zeropage_x(bus, pc))),
        0xd6 => String::from(format!("{:04x}  dec {}", pc,  zeropage_x(bus, pc))),
        0xd8 => String::from(format!("{:04x}  cld {}", pc,  implied(bus, pc))),
        0xd9 => String::from(format!("{:04x}  cmp {}", pc,  absolute_y(bus, pc))),
        0xda => String::from(format!("{:04x}  phx {}", pc,  implied(bus, pc))),
        0xdd => String::from(format!("{:04x}  cmp {}", pc,  absolute_x(bus, pc))),
        0xde => String::from(format!("{:04x}  dec {}", pc,  absolute_x(bus, pc))),
        0xe0 => String::from(format!("{:04x}  cpx {}", pc,  immediate(bus, pc))),
        0xe1 => String::from(format!("{:04x}  sbc {}", pc,  indirect_x(bus, pc))),
        0xe4 => String::from(format!("{:04x}  cpx {}", pc,  zeropage(bus, pc))),
        0xe5 => String::from(format!("{:04x}  sbc {}", pc,  zeropage(bus, pc))),
        0xe6 => String::from(format!("{:04x}  inc {}", pc,  zeropage(bus, pc))),
        0xe8 => String::from(format!("{:04x}  inx {}", pc,  implied(bus, pc))),
        0xe9 => String::from(format!("{:04x}  sbc {}", pc,  immediate(bus, pc))),
        0xea => String::from(format!("{:04x}  nop {}", pc,  implied(bus, pc))),
        0xec => String::from(format!("{:04x}  cpx {}", pc,  absolute(bus, pc))),
        0xed => String::from(format!("{:04x}  sbc {}", pc,  absolute(bus, pc))),
        0xee => String::from(format!("{:04x}  inc {}", pc,  absolute(bus, pc))),
        0xf0 => String::from(format!("{:04x}  beq {}", pc,  branch(bus, pc))),
        0xf1 => String::from(format!("{:04x}  sbc {}", pc,  indirect_y(bus, pc))),
        0xf5 => String::from(format!("{:04x}  sbc {}", pc,  zeropage_x(bus, pc))),
        0xf6 => String::from(format!("{:04x}  inc {}", pc,  zeropage_x(bus, pc))),
        0xf8 => String::from(format!("{:04x}  sed {}", pc,  implied(bus, pc))),
        0xf9 => String::from(format!("{:04x}  sbc {}", pc,  absolute_y(bus, pc))),
        0xfa => String::from(format!("{:04x}  plx {}", pc,  implied(bus, pc))),
        0xfd => String::from(format!("{:04x}  sbc {}", pc,  absolute_x(bus, pc))),
        0xfe => String::from(format!("{:04x}  inc {}", pc,  absolute_x(bus, pc))),

        _ => String::from(format!("{:04x}  ${:02x}", pc, bus.read_debugger(pc))),
    }
}


fn absolute<T: CpuBus>(bus: &mut T, pc: u16) -> String {
    format!("${:02x}{:02x}", bus.read_debugger(pc + 2), bus.read_debugger(pc + 1))
}

fn absolute_x<T: CpuBus>(bus: &mut T, pc: u16) -> String {
    format!("${:02x}{:02x},x", bus.read_debugger(pc + 2), bus.read_debugger(pc + 1))
}

fn absolute_y<T: CpuBus>(bus: &mut T, pc: u16) -> String {
    format!("${:02x}{:02x},y", bus.read_debugger(pc + 2), bus.read_debugger(pc + 1))
}

fn branch<T: CpuBus>(bus: &mut T, pc: u16) -> String {
    format!("${:02x}", bus.read_debugger(pc + 1))
}

fn immediate<T: CpuBus>(bus: &mut T, pc: u16) -> String {
    format!("#${:02x}", bus.read_debugger(pc + 1))
}

fn implied<T: CpuBus>(_bus: &mut T, _pc: u16) -> String {
    format!("")
}

fn indirect<T: CpuBus>(bus: &mut T, pc: u16) -> String {
    format!("(${:02x}{:02x})", bus.read_debugger(pc + 2), bus.read_debugger(pc + 1))
}

fn indirect_x<T: CpuBus>(bus: &mut T, pc: u16) -> String {
    format!("(${:02x}{:02x},x)", bus.read_debugger(pc + 2), bus.read_debugger(pc + 1))
}

fn indirect_y<T: CpuBus>(bus: &mut T, pc: u16) -> String {
    format!("(${:02x}{:02x},y)", bus.read_debugger(pc + 2), bus.read_debugger(pc + 1))
}

fn zeropage<T: CpuBus>(bus: &mut T, pc: u16) -> String {
    format!("${:02x}", bus.read_debugger(pc + 1))
}

fn zeropage_x<T: CpuBus>(bus: &mut T, pc: u16) -> String {
    format!("${:02x},x", bus.read_debugger(pc + 1))
}

fn zeropage_y<T: CpuBus>(bus: &mut T, pc: u16) -> String {
    format!("${:02x},y", bus.read_debugger(pc + 1))
}