use super::mos6502::{Mos6502, CpuBus, Status};
use super::instructions::*;
use super::algorithms::*;
use super::memory::*;

impl Mos6502 {
    pub fn instruction(&mut self, bus: &mut impl CpuBus) {
        let code = opcode(self, bus);
        let n = self.r.n;
        let v = self.r.v;
        let c = self.r.c;
        let z = self.r.z;
        let x = self.r.x;
        let y = self.r.y;
        let a = self.r.a;
        let s = self.r.s;

        match code {
            0x00 => break_(self, bus),  // break is a keyword 
            0x01 => self.r.a = indirect_x_read(self, bus, ora),
            0x05 => self.r.a = zero_page_read(self, bus, ora),
            0x06 => zero_page_modify(self, bus, asl),
            0x08 => push_p(self, bus),            
            0x09 => self.r.a = immediate(self, bus, ora),
            0x0a => self.r.a = implied(self, bus, asl, a),
            0x0d => self.r.a = absolute_read(self, bus, ora),
            0x0e => absolute_modify(self, bus, asl),
            0x10 => branch(self, bus, n == false),
            0x11 => self.r.a = indirect_y_read(self, bus, ora),
            0x15 => self.r.a = zero_page_read_index(self, bus, ora, x),
            0x16 => zero_page_modify_index(self, bus, asl, x),
            0x18 => clear(self, bus, Status::C),
            0x19 => self.r.a = absolute_read_index(self, bus, ora, y),
            0x1d => self.r.a = absolute_read_index(self, bus, ora, x),
            0x1e => absolute_modify_index(self, bus, asl, x),
            0x20 => call_absolute(self, bus),
            0x21 => self.r.a = indirect_x_read(self, bus, and),
            0x24 => { zero_page_read(self, bus, bit); },
            0x25 => self.r.a = zero_page_read(self, bus, and),
            0x26 => zero_page_modify(self, bus, rol),
            0x28 => pull_p(self, bus),                        
            0x29 => self.r.a = immediate(self, bus, and),
            0x2a => self.r.a = implied(self, bus, rol, a),
            0x2c => { absolute_read(self, bus, bit); },
            0x2d => self.r.a = absolute_read(self, bus, and),
            0x2e => absolute_modify(self, bus, rol),
            0x30 => branch(self, bus, n == true),
            0x31 => self.r.a = indirect_y_read(self, bus, and),
            0x35 => self.r.a = zero_page_read_index(self, bus, and, x),
            0x36 => zero_page_modify_index(self, bus, rol, x),
            0x38 => set(self, bus, Status::C),
            0x39 => self.r.a = absolute_read_index(self, bus, and, y),
            0x3d => self.r.a = absolute_read_index(self, bus, and, x),
            0x3e => absolute_modify_index(self, bus, rol, x),
            0x40 => return_interrupt(self, bus),
            0x41 => self.r.a = indirect_x_read(self, bus, eor),
            0x45 => self.r.a = zero_page_read(self, bus, eor),
            0x46 => zero_page_modify(self, bus, lsr),
            0x48 => push(self, bus, a),
            0x49 => self.r.a = immediate(self, bus, eor),
            0x4a => self.r.a = implied(self, bus, lsr, a),
            0x4c => jump_absolute(self, bus),
            0x4d => self.r.a = absolute_read(self, bus, eor),
            0x4e => absolute_modify(self, bus, lsr),
            0x50 => branch(self, bus, v == false),
            0x51 => self.r.a = indirect_y_read(self, bus, eor),
            0x55 => self.r.a = zero_page_read_index(self, bus, eor, x),
            0x56 => zero_page_modify_index(self, bus, lsr, x),
            0x58 => clear(self, bus, Status::I),
            0x59 => self.r.a = absolute_read_index(self, bus, eor, y),
            0x5a => push(self, bus, y),     // phy
            0x5d => self.r.a = absolute_read_index(self, bus, eor, x),
            0x5e => absolute_modify_index(self, bus, lsr, x),
            0x60 => return_subroutine(self, bus),
            0x61 => self.r.a = indirect_x_read(self, bus, adc),
            0x65 => self.r.a = zero_page_read(self, bus, adc),
            0x64 => zero_page_write(self, bus, 0),      // stz
            0x66 => zero_page_modify(self, bus, ror),
            0x68 => self.r.a = pull(self, bus),
            0x69 => self.r.a = immediate(self, bus, adc),
            0x6a => self.r.a = implied(self, bus, ror, a),
            0x6c => jump_indirect(self, bus),
            0x6d => self.r.a = absolute_read(self, bus, adc),
            0x6e => absolute_modify(self, bus, ror),
            0x70 => branch(self, bus, v == true),
            0x71 => self.r.a = indirect_y_read(self, bus, adc),
            0x74 => zero_page_write_index(self, bus, 0, x),      // stz
            0x75 => self.r.a = zero_page_read_index(self, bus, adc, x),
            0x76 => zero_page_modify_index(self, bus, ror, x),
            0x78 => set(self, bus, Status::I),
            0x79 => self.r.a = absolute_read_index(self, bus, adc, y),
            0x7a => self.r.y = pull(self, bus),     // ply
            0x7d => self.r.a = absolute_read_index(self, bus, adc, x),
            0x7e => absolute_modify_index(self, bus, ror, x),
            0x80 => branch(self, bus, true),    // bra
            0x81 => indirect_x_write(self, bus, a),
            0x84 => zero_page_write(self, bus, y),
            0x85 => zero_page_write(self, bus, a),
            0x86 => zero_page_write(self, bus, x),
            0x88 => self.r.y = implied(self, bus, dec, y),
            0x8a => self.r.a = transfer(self, bus, x, true),
            0x8c => absolute_write(self, bus, y),
            0x8d => absolute_write(self, bus, a),
            0x8e => absolute_write(self, bus, x),
            0x90 => branch(self, bus, c == false),
            0x91 => indirect_y_write(self, bus, a),
            0x94 => zero_page_write_index(self, bus, y, x),
            0x95 => zero_page_write_index(self, bus, a, x),
            0x96 => zero_page_write_index(self, bus, x, y),
            0x98 => self.r.a = transfer(self, bus, y, true),
            0x99 => absolute_write_index(self, bus, a, y),
            0x9a => self.r.s = transfer(self, bus, x, false),
            0x9c => absolute_write(self, bus, 0),   // stz
            0x9d => absolute_write_index(self, bus, a, x),
            0x9e => absolute_write_index(self, bus, 0, x),   // stz
            0xa0 => self.r.y = immediate(self, bus, ld),
            0xa1 => self.r.a = indirect_x_read(self, bus, ld),
            0xa2 => self.r.x = immediate(self, bus, ld),
            0xa4 => self.r.y = zero_page_read(self, bus, ld),
            0xa5 => self.r.a = zero_page_read(self, bus, ld),
            0xa6 => self.r.x = zero_page_read(self, bus, ld),
            0xa8 => self.r.y = transfer(self, bus, a, true),
            0xa9 => self.r.a = immediate(self, bus, ld),
            0xaa => self.r.x = transfer(self, bus, a, true),
            0xac => self.r.y = absolute_read(self, bus, ld),
            0xad => self.r.a = absolute_read(self, bus, ld),
            0xae => self.r.x = absolute_read(self, bus, ld),
            0xb0 => branch(self, bus, c == true),
            0xb1 => self.r.a = indirect_y_read(self, bus, ld),
            0xb4 => self.r.y = zero_page_read_index(self, bus, ld, x),
            0xb5 => self.r.a = zero_page_read_index(self, bus, ld, x),
            0xb6 => self.r.x = zero_page_read_index(self, bus, ld, y),
            0xb8 => clear(self, bus, Status::V),
            0xb9 => self.r.a = absolute_read_index(self, bus, ld, y),
            0xba => self.r.x = transfer(self, bus, s, true),
            0xbc => self.r.y = absolute_read_index(self, bus, ld, x),
            0xbd => self.r.a = absolute_read_index(self, bus, ld, x),
            0xbe => self.r.x = absolute_read_index(self, bus, ld, y),
            0xc0 => { immediate(self, bus, cpy); },
            0xc1 => { indirect_x_read(self, bus, cmp); },
            0xc4 => { zero_page_read(self, bus, cpy); },
            0xc5 => { zero_page_read(self, bus, cmp); },
            0xc6 => zero_page_modify(self, bus, dec),
            0xc8 => self.r.y = implied(self, bus, inc, y),
            0xc9 => { immediate(self, bus, cmp); },
            0xca => self.r.x = implied(self, bus, dec, x),
            0xcc => { absolute_read(self, bus, cpy); },
            0xcd => { absolute_read(self, bus, cmp); },
            0xce => absolute_modify(self, bus, dec),
            0xd0 => branch(self, bus, z == false),
            0xd1 => { indirect_y_read(self, bus, cmp); },
            0xd5 => { zero_page_read_index(self, bus, cmp, x); },
            0xd6 => zero_page_modify_index(self, bus, dec, x),
            0xd8 => clear(self, bus, Status::D),
            0xd9 => { absolute_read_index(self, bus, cmp, y); },
            0xdd => { absolute_read_index(self, bus, cmp, x); },
            0xde => absolute_modify_index(self, bus, dec, x),
            0xda => push(self, bus, x),     // phx
            0xe0 => { immediate(self, bus, cpx); },
            0xe1 => self.r.a = indirect_x_read(self, bus, sbc),
            0xe4 => { zero_page_read(self, bus, cpx); },
            0xe5 => self.r.a = zero_page_read(self, bus, sbc),
            0xe6 => zero_page_modify(self, bus, inc),
            0xe8 => self.r.x = implied(self, bus, inc, x),
            0xe9 => self.r.a = immediate(self, bus, sbc),
            0xea => nop(self, bus),
            0xec => { absolute_read(self, bus, cpx); }
            0xed => self.r.a = absolute_read(self, bus, sbc),
            0xee => absolute_modify(self, bus, inc),
            0xf0 => branch(self, bus, z == true),
            0xf1 => self.r.a = indirect_y_read(self, bus, sbc),
            0xf5 => self.r.a = zero_page_read_index(self, bus, sbc, x),
            0xf6 => zero_page_modify_index(self, bus, inc, x),
            0xf8 => set(self, bus, Status::D),
            0xf9 => self.r.a = absolute_read_index(self, bus, sbc, y),
            0xfa => self.r.x = pull(self, bus),     // plx
            0xfd => self.r.a = absolute_read_index(self, bus, sbc, x),
            0xfe => absolute_modify_index(self, bus, inc, x),
            _ => println!("Opcode not implemented: 0x{:x}", code),
        }
    }
}