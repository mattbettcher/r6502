#![allow(dead_code)]
use std::fmt;

// defines how the cpu talks to memory and memory mapped devices
pub trait CpuBus {
    fn read(&mut self, addr: u16) -> u8; // read a byte from the provided address
    fn write(&mut self, addr: u16, data: u8); // write a byte to the provided address
    fn read_debugger(&mut self, addr: u16) -> u8; // read a byte for use by the debugger
}

pub enum Status {
    C,
    I,
    D,
    V,
}

pub struct Registers {
    pub a: u8,   // accumulator
    pub x: u8,   // x index
    pub y: u8,   // y index
    pub s: u8,   // stack
    pub pc: u16, // program counter
    pub c: bool,
    pub z: bool,
    pub i: bool,
    pub d: bool,
    pub v: bool,
    pub n: bool,
    pub mdr: u8, // memory data register
}

pub struct Mos6502 {
    pub r: Registers,
    pub bcd: bool, //set to false to disable BCD mode in ADC, SBC instructions
    pub last_cycle: fn(),
    pub nmi: fn(vector: &mut u16),
}

pub type AluOp = fn(&mut Mos6502, u8) -> u8;

impl fmt::Display for Mos6502 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:4x}", self.r.pc)
    }
}

impl Mos6502 {
    pub fn power(lc: fn(), nmi: fn(&mut u16)) -> Self {
        Mos6502 {
            r: Registers {
                a: 0,
                x: 0,
                y: 0,
                s: 0xff,
                mdr: 0,
                pc: 0,
                c: false,
                z: false,
                i: false,
                d: false,
                v: false,
                n: false,
            },
            bcd: false,
            last_cycle: lc,
            nmi: nmi,
        }
    }

    pub fn set_z_n(&mut self, data: u8) {
        self.r.z = data == 0;
        self.r.n = (data >> 7) == 1;
    }

    pub fn pack_status(&mut self) -> u8 {
        (self.r.c as u8) << 0 | (self.r.z as u8) << 1 | (self.r.i as u8) << 2
            | (self.r.d as u8) << 3 | (self.r.v as u8) << 6 | (self.r.n as u8) << 7 | 0x30
    }

    pub fn unpack_status(&mut self, data: u8) {
        self.r.c = (data >> 0) & 0x01 == 1;
        self.r.z = (data >> 1) & 0x01 == 1;
        self.r.i = (data >> 2) & 0x01 == 1;
        self.r.d = (data >> 3) & 0x01 == 1;
        self.r.v = (data >> 6) & 0x01 == 1;
        self.r.n = (data >> 7) & 0x01 == 1;
    }
}
