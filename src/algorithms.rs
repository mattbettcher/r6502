use super::mos6502::Mos6502;
use std::u32;

pub fn adc(cpu: &mut Mos6502, i: u8) -> u8 {
    let mut o: u32;
    let c: u32;
    if cpu.r.c { c = 1; } else { c = 0; }
    if !cpu.bcd || !cpu.r.d {
        o = (cpu.r.a as u32) + c + (i as u32);
        // set V
        let v = (!(cpu.r.a  ^ i) & (cpu.r.a ^ o as u8)) & 0x80;
        if v > 0 { cpu.r.v = true; } else { cpu.r.v = false; }
    } else {
        //self.idle(bus); missing bus !!! this will be a cycle off for now
        o = (cpu.r.a as u32 & 0x0f) + (i as u32 & 0x0f) + (c << 0);
        if o > 0x09 { o += 0x06; }
        cpu.r.c = o > 0x0f;
        o = (cpu.r.a as u32 & 0xf0) + (i as u32 & 0xf0) + (c << 4) + (o & 0x0f);
        if o > 0x9f { o += 0x60; }
    }
    cpu.r.c = o >> 8 == 1;
    cpu.set_z_n(o as u8);
    o as u8
}

pub fn and(cpu: &mut Mos6502, i: u8) -> u8 {
    let o = cpu.r.a & i;
    cpu.set_z_n(o);
    o
}

pub fn asl(cpu: &mut Mos6502, i: u8) -> u8 {
    let o = i << 1;
    cpu.r.c = i >> 7 == 1;
    cpu.set_z_n(o);
    o
}

pub fn bit(cpu: &mut Mos6502, i: u8) -> u8 {
    cpu.r.z = cpu.r.a & i == 0;
    cpu.r.v = (i >> 6) & 0x1 == 1;
    cpu.r.n = (i >> 7) & 0x1 == 1;
    cpu.r.a
}

pub fn cmp(cpu: &mut Mos6502, i: u8) -> u8 {
    let o = cpu.r.a.wrapping_sub(i);
    cpu.r.c = cpu.r.a >= i;
    cpu.set_z_n(o as u8);
    cpu.r.z = cpu.r.a == i;
    cpu.r.a
}

pub fn cpx(cpu: &mut Mos6502, i: u8) -> u8 {
    let o = cpu.r.x.wrapping_sub(i);
    cpu.r.c = cpu.r.x >= i;
    cpu.set_z_n(o as u8);
    cpu.r.z = cpu.r.x == i;
    cpu.r.x
}

pub fn cpy(cpu: &mut Mos6502, i: u8) -> u8 {
    let o = cpu.r.y.wrapping_sub(i);
    cpu.r.c = cpu.r.y >= i;
    cpu.set_z_n(o as u8);
    cpu.r.z = cpu.r.y == i;
    cpu.r.y
}

pub fn dec(cpu: &mut Mos6502, i: u8) -> u8 {
    let o = i.wrapping_sub(1);
    cpu.set_z_n(o);
    o
}

pub fn eor(cpu: &mut Mos6502, i: u8) -> u8 {
    let o = cpu.r.a ^ i;
    cpu.set_z_n(o);
    o
}

pub fn inc(cpu: &mut Mos6502, i: u8) -> u8 {
    let o = i.wrapping_add(1);
    cpu.set_z_n(o);
    o
}

pub fn ld(cpu: &mut Mos6502, i: u8) -> u8 {
    cpu.set_z_n(i);
    i
}

pub fn lsr(cpu: &mut Mos6502, i: u8) -> u8 {
    let o = i >> 1;
    cpu.r.c = i & 0x01 == 1;
    cpu.set_z_n(o);
    o
}

pub fn ora(cpu: &mut Mos6502, i: u8) -> u8 {
    let o = cpu.r.a | i;
    cpu.set_z_n(o);
    o
}

pub fn rol(cpu: &mut Mos6502, i: u8) -> u8 {
    let c: u8;
    if cpu.r.c { c = 1; } else { c = 0; }
    cpu.r.c = i >> 7 == 1;
    let o = i << 1 | c;
    cpu.set_z_n(o);
    o
}

pub fn ror(cpu: &mut Mos6502, i: u8) -> u8 {
    let c: u8;
    if cpu.r.c { c = 1; } else { c = 0; }
    cpu.r.c = i & 0x01 == 1;
    let o = c << 7 | i >> 1;
    cpu.set_z_n(o);
    o
}

pub fn sbc(cpu: &mut Mos6502, i: u8) -> u8 {
    let _i = !i;
    let mut o: u32;
    let c: u32;
    if cpu.r.c { c = 1; } else { c = 0; }
    if !cpu.bcd || !cpu.r.d {
        o = (cpu.r.a as u32) + c + (_i as u32);
        // set V
        let v = (!(cpu.r.a  ^ _i) & (cpu.r.a ^ o as u8)) & 0x80;
        if v > 0 { cpu.r.v = true; } else { cpu.r.v = false; }
    } else {
        //self.idle(bus); missing bus !!! this will be a cycle off for now
        o = (cpu.r.a as u32 & 0x0f) + (i as u32 & 0x0f) + (c << 0);
        if o > 0x09 { o -= 0x06; }
        cpu.r.c = o <= 0x0f;
        o = (cpu.r.a as u32 & 0xf0) + (i as u32 & 0xf0) + (c << 4) + (o & 0x0f);
        if o <= 0xff { o -= 0x60; }
    }
    cpu.r.c = o >> 8 == 1;
    cpu.set_z_n(o as u8);
    o as u8
}