use super::mos6502::{Mos6502, CpuBus};

pub fn opcode(cpu: &mut Mos6502, bus: &mut impl CpuBus) -> u8 {
    cpu.r.pc += 1;
    bus.read(cpu.r.pc - 1)     // rust dosen't have postfix operators
}

pub fn operand(cpu: &mut Mos6502, bus: &mut impl CpuBus) -> u8 {
    cpu.r.pc += 1;
    bus.read(cpu.r.pc - 1)     // rust dosen't have postfix operators
}

pub fn idle_page_crossed(bus: &mut impl CpuBus, x: u16, y: u16) {
    if x >> 8 == y >> 8 { return }
    let _ = bus.read((x & 0xff00) | (y & 0x00ff));
}

pub fn idle_page_always(bus: &mut impl CpuBus, x: u16, y: u16) {
    let _ = bus.read((x & 0xff00) | (y & 0x00ff));
}

pub fn load(bus: &mut impl CpuBus, addr: u16) -> u8 {
    bus.read(addr)
}

pub fn store(bus: &mut impl CpuBus, addr: u16, data: u8) {
    bus.write(addr, data);
}

pub fn push_s(cpu: &mut Mos6502, bus: &mut impl CpuBus, data: u8) {
    bus.write(0x0100 | cpu.r.s as u16, data);
    cpu.r.s = cpu.r.s.wrapping_sub(1);              // rust dosen't have postfix operators
}

pub fn pull_s(cpu: &mut Mos6502, bus: &mut impl CpuBus) -> u8 {
    cpu.r.s = cpu.r.s.wrapping_add(1);              // rust dosen't have postfix operators
    bus.read(0x0100 | cpu.r.s as u16)
}

pub fn idle(cpu: &mut Mos6502, bus: &mut impl CpuBus) {
    let _ = bus.read(cpu.r.pc);
}