use super::mos6502::{Mos6502, CpuBus, AluOp, Status};
use super::memory::*;

pub fn absolute_modify(cpu: &mut Mos6502, bus: &mut impl CpuBus, alu: AluOp) {
    let mut absolute: u16 = operand(cpu, bus) as u16;
    absolute |= (operand(cpu, bus) as u16) << 8;
    let data = bus.read(absolute);
    bus.write(absolute, data);
    (cpu.last_cycle)();
    let o = (alu)(cpu, data);
    bus.write(absolute, o);
}

pub fn absolute_modify_index(cpu: &mut Mos6502, bus: &mut impl CpuBus, alu: AluOp, index: u8) {
    let mut absolute: u16 = operand(cpu, bus) as u16;
    absolute |= (operand(cpu, bus) as u16) << 8;
    idle_page_always(bus, absolute, absolute + index as u16);
    let data = bus.read(absolute + index as u16);
    bus.write(absolute + index as u16, data);
    (cpu.last_cycle)();
    let o = (alu)(cpu, data);
    bus.write(absolute + index as u16, o);
}

pub fn absolute_read(cpu: &mut Mos6502, bus: &mut impl CpuBus, alu: AluOp) -> u8 {
    let mut absolute: u16 = operand(cpu, bus) as u16;
    absolute |= (operand(cpu, bus) as u16) << 8;
    (cpu.last_cycle)();
    let read = bus.read(absolute);
    (alu)(cpu, read)
}

pub fn absolute_read_index(cpu: &mut Mos6502, bus: &mut impl CpuBus, alu: AluOp, index: u8) -> u8 {
    let mut absolute: u16 = operand(cpu, bus) as u16;
    absolute |= (operand(cpu, bus) as u16) << 8;
    idle_page_crossed(bus, absolute, absolute + index as u16);
    (cpu.last_cycle)();
    let read = bus.read(absolute + index as u16);
    (alu)(cpu, read)
}

pub fn absolute_write(cpu: &mut Mos6502, bus: &mut impl CpuBus, data: u8) {
    let mut absolute: u16 = operand(cpu, bus) as u16;
    absolute |= (operand(cpu, bus) as u16) << 8;
    (cpu.last_cycle)();
    bus.write(absolute, data);
}

pub fn absolute_write_index(cpu: &mut Mos6502, bus: &mut impl CpuBus, data: u8, index: u8) {
    let mut absolute: u16 = operand(cpu, bus) as u16;
    absolute |= (operand(cpu, bus) as u16) << 8;
    idle_page_always(bus, absolute, absolute + index as u16);
    (cpu.last_cycle)();
    bus.write(absolute + index as u16, data);
}

pub fn branch(cpu: &mut Mos6502, bus: &mut impl CpuBus, take: bool) {
    if !take {
        (cpu.last_cycle)();
        operand(cpu, bus);
    } else {
        let displacement = operand(cpu, bus) as i8;
        let pc = cpu.r.pc;
        idle_page_crossed(bus, pc, ((pc as i16) + (displacement as i16)) as u16);
        (cpu.last_cycle)();
        idle(cpu, bus);
        cpu.r.pc = ((cpu.r.pc as i16) + (displacement as i16)) as u16;
    }
}

pub fn break_(cpu: &mut Mos6502, bus: &mut impl CpuBus) {
    let pc = cpu.r.pc + 1;
    operand(cpu, bus);
    push(cpu, bus, (pc >> 8) as u8);
    push(cpu, bus, (pc & 0x00ff) as u8);
    let mut vector = 0xfffe;
    (cpu.nmi)(&mut vector);
    let p = cpu.pack_status() | 0x30;
    push(cpu, bus, p);
    cpu.r.i = true;
    cpu.r.pc = 0;      // clear pc
    cpu.r.pc |= bus.read(vector) as u16;
    vector += 1;  // no post inc in rust
    (cpu.last_cycle)();
    cpu.r.pc |= (bus.read(vector) as u16) << 8;
}

pub fn call_absolute(cpu: &mut Mos6502, bus: &mut impl CpuBus) {
    let mut absolute: u16 = operand(cpu, bus) as u16;
    absolute |= (operand(cpu, bus) as u16) << 8;
    idle(cpu, bus);
    cpu.r.pc -= 1;
    let pc = cpu.r.pc;
    push(cpu, bus, (pc >> 8) as u8);
    (cpu.last_cycle)();
    push(cpu, bus, (pc & 0x00ff) as u8);
    cpu.r.pc = absolute;
}

pub fn immediate(cpu: &mut Mos6502, bus: &mut impl CpuBus, alu: AluOp) -> u8 {
    (cpu.last_cycle)();
    let data = operand(cpu, bus);
    (alu)(cpu, data)
}

pub fn implied(cpu: &mut Mos6502, bus: &mut impl CpuBus, alu: AluOp, data: u8) -> u8 {
    (cpu.last_cycle)();
    idle(cpu, bus);
    (alu)(cpu, data)
}

pub fn indirect_x_read(cpu: &mut Mos6502, bus: &mut impl CpuBus, alu: AluOp) -> u8 {
    let zero_page = operand(cpu, bus);
    load(bus, zero_page as u16);
    let x = cpu.r.x;
    let mut absolute = load(bus, zero_page.wrapping_add(x + 0) as u16) as u16;
    absolute |= (load(bus, zero_page.wrapping_add(x + 1) as u16) as u16) << 8;
    (cpu.last_cycle)();
    let o = bus.read(absolute);
    (alu)(cpu, o)
}

pub fn indirect_x_write(cpu: &mut Mos6502, bus: &mut impl CpuBus, data: u8) {
    let zero_page = operand(cpu, bus);
    load(bus, zero_page as u16);
    let x = cpu.r.x;
    let mut absolute = load(bus, zero_page.wrapping_add(x + 0) as u16) as u16;
    absolute |= (load(bus, zero_page.wrapping_add(x + 1) as u16) as u16) << 8;
    (cpu.last_cycle)();
    bus.write(absolute, data);
}

pub fn indirect_y_read(cpu: &mut Mos6502, bus: &mut impl CpuBus, alu: AluOp) -> u8{
    let zero_page = operand(cpu, bus);
    let mut absolute = load(bus, zero_page as u16) as u16;
    absolute |= (load(bus, (zero_page + 1) as u16) as u16) << 8;
    let y = cpu.r.y;
    idle_page_crossed(bus, absolute, absolute + y as u16);
    (cpu.last_cycle)();
    let o = bus.read(absolute + y as u16);
    (alu)(cpu, o)
}

pub fn indirect_y_write(cpu: &mut Mos6502, bus: &mut impl CpuBus, data: u8) {
    let zero_page = operand(cpu, bus) as u16;
    let mut absolute = load(bus, zero_page + 0) as u16;
    absolute |= (load(bus, zero_page + 1) as u16) << 8;
    let y = cpu.r.y as u16;
    idle_page_always(bus, absolute, absolute + y);
    (cpu.last_cycle)();
    bus.write(absolute + y, data);
}

pub fn jump_absolute(cpu: &mut Mos6502, bus: &mut impl CpuBus) {
    let mut absolute = operand(cpu, bus) as u16;
    (cpu.last_cycle)();
    absolute |= (operand(cpu, bus) as u16) << 8;
    cpu.r.pc = absolute;
}

pub fn jump_indirect(cpu: &mut Mos6502, bus: &mut impl CpuBus) {
    let mut absolute = operand(cpu, bus) as u16;
    absolute |= (operand(cpu, bus) as u16) << 8;
    let mut pc = bus.read(absolute) as u16;
    if absolute == 0xff { absolute = 0; } else { absolute += 1; }
    (cpu.last_cycle)();
    pc |= (bus.read(absolute) as u16) << 8;
    cpu.r.pc = pc;
}

pub fn nop(cpu: &mut Mos6502, bus: &mut impl CpuBus) {
    (cpu.last_cycle)();
    idle(cpu, bus);
}

pub fn zero_page_modify(cpu: &mut Mos6502, bus: &mut impl CpuBus, alu: AluOp) {
    let zero_page = operand(cpu, bus) as u16;
    let mut data = load(bus, zero_page);
    store(bus,zero_page, data);
    (cpu.last_cycle)();
    data = (alu)(cpu, data);
    store(bus, zero_page, data);
}

pub fn zero_page_modify_index(cpu: &mut Mos6502, bus: &mut impl CpuBus, alu: AluOp, index: u8) {
    let zero_page = operand(cpu, bus);
    load(bus, zero_page as u16);
    let mut data = load(bus, zero_page.wrapping_add(index) as u16);
    store(bus, zero_page.wrapping_add(index) as u16, data);
    (cpu.last_cycle)();
    data = (alu)(cpu, data);
    store(bus, zero_page.wrapping_add(index) as u16, data);
}

pub fn zero_page_read(cpu: &mut Mos6502, bus: &mut impl CpuBus, alu: AluOp) -> u8 {
    let zero_page = operand(cpu, bus) as u16;
    load(bus, zero_page);
    (cpu.last_cycle)();
    let o = load(bus, zero_page);
    (alu)(cpu, o)
}

pub fn zero_page_read_index(cpu: &mut Mos6502, bus: &mut impl CpuBus, alu: AluOp, index: u8) -> u8 {
    let zero_page = operand(cpu, bus);
    load(bus, zero_page as u16);
    (cpu.last_cycle)();
    let o = load(bus, zero_page.wrapping_add(index) as u16);
    (alu)(cpu, o)
}

pub fn zero_page_write(cpu: &mut Mos6502, bus: &mut impl CpuBus, data: u8) {
    let zero_page = operand(cpu, bus) as u16;
    (cpu.last_cycle)();
    store(bus, zero_page, data);
}

pub fn zero_page_write_index(cpu: &mut Mos6502, bus: &mut impl CpuBus, data: u8, index: u8) {
    let zero_page = operand(cpu, bus);
    bus.read(zero_page as u16);
    (cpu.last_cycle)();
    store(bus, zero_page.wrapping_add(index) as u16, data);
}

pub fn clear(cpu: &mut Mos6502, bus: &mut impl CpuBus, flag: Status) {
    (cpu.last_cycle)();
    idle(cpu, bus);
    match flag {
        Status::C => cpu.r.c = false,
        Status::I => cpu.r.i = false,
        Status::D => cpu.r.d = false,
        Status::V => cpu.r.v = false
    }
}

pub fn set(cpu: &mut Mos6502, bus: &mut impl CpuBus, flag: Status) {
    (cpu.last_cycle)();
    idle(cpu, bus);
    match flag {
        Status::C => cpu.r.c = true,
        Status::I => cpu.r.i = true,
        Status::D => cpu.r.d = true,
        Status::V => cpu.r.v = true
    }
}

pub fn pull_p(cpu: &mut Mos6502, bus: &mut impl CpuBus) {
    idle(cpu, bus);
    idle(cpu, bus);
    (cpu.last_cycle)();
    let p = pull_s(cpu, bus);
    cpu.unpack_status(p);
}

pub fn push_p(cpu: &mut Mos6502, bus: &mut impl CpuBus) {
    idle(cpu, bus);
    (cpu.last_cycle)();
    let p = cpu.pack_status();
    push(cpu, bus, p | 0x30);       // set both break & reserved flags
}

pub fn pull(cpu: &mut Mos6502, bus: &mut impl CpuBus) -> u8 {
    idle(cpu, bus);
    idle(cpu, bus);
    (cpu.last_cycle)();
    let data = pull_s(cpu, bus);
    cpu.set_z_n(data);
    data
}

pub fn push(cpu: &mut Mos6502, bus: &mut impl CpuBus, data: u8) {
    idle(cpu, bus);
    (cpu.last_cycle)();
    push_s(cpu, bus, data);
}

pub fn return_subroutine(cpu: &mut Mos6502, bus: &mut impl CpuBus) {
    idle(cpu, bus);
    idle(cpu, bus);
    cpu.r.pc &= 0xff00;        // clear bottom
    cpu.r.pc |= pull_s(cpu, bus) as u16;
    cpu.r.pc &= 0x00ff;        // clear top
    cpu.r.pc |= (pull_s(cpu, bus) as u16) << 8;
    (cpu.last_cycle)();
    idle(cpu, bus);
    cpu.r.pc += 1;
}

pub fn return_interrupt(cpu: &mut Mos6502, bus: &mut impl CpuBus) {
    idle(cpu, bus);
    idle(cpu, bus);
    let p = pull_s(cpu, bus);
    cpu.unpack_status(p);
    cpu.r.pc &= 0xff00;        // clear bottom
    cpu.r.pc |= pull_s(cpu, bus) as u16;
    (cpu.last_cycle)();
    cpu.r.pc &= 0x00ff;        // clear top
    cpu.r.pc |= (pull_s(cpu, bus) as u16) << 8;
}

pub fn transfer(cpu: &mut Mos6502, bus: &mut impl CpuBus, source: u8, flag: bool) -> u8 {
    (cpu.last_cycle)();
    idle(cpu, bus);
    if flag { cpu.set_z_n(source); }
    source
}