pub mod mos6502;
pub mod algorithms;
pub mod instructions;
pub mod instruction;
pub mod memory;
pub mod disassembler;

#[cfg(test)]
mod tests {

    // testing an entire 6502 cpu is a not hard in rust!
    use super::mos6502::*;
    use super::disassembler::*;
    use std::fs::File;
    use std::io::Read;
    use std::error::Error;

    fn load(file_path: &str) -> Vec<u8> {
        let mut file = match File::open(file_path) {
            Err(why) => panic!("Couldn't open {}: {}", file_path, why.description()),
            Ok(file) => file,
        };
        // Read the whole thing
        let mut cartridge = Vec::new();
        match file.read_to_end(&mut cartridge) {
            Err(why) => panic!("Couldn't read data: {}", why.description()),
            Ok(bytes_read) => {
                println!("Data read successfully: {}", bytes_read);
                cartridge
            }
        }
    }

    pub struct Mem {
        pub ram: Vec<u8>,
    }

    impl CpuBus for Mem {
        fn read(&mut self, addr: u16) -> u8 {
            self.ram[addr as usize]
        }
        fn write(&mut self, addr: u16, data: u8) {
            self.ram[addr as usize] = data;
        }
        fn read_debugger(&mut self, addr: u16) -> u8 {
            self.ram[addr as usize]
        }
    }

    fn last_cycle() {}
    fn nmi(_vector: &mut u16) {}

    #[test]
    fn functional_test() {

        let mut cpu = Mos6502::power(last_cycle, nmi);
        let mut mem = Mem { ram: load("6502_functional_test.bin") };
        cpu.r.pc = 0x0400;  // prog start address

        let mut done = false;
        while !done  {
            for _i in 0..10000 {
            //println!("A:{:02x} X:{:02x} Y:{:02x} S:{:02x} {:}", cpu.r.a, cpu.r.x, cpu.r.y, cpu.r.s, disassemble(&mut mem, cpu.r.pc));
                if cpu.r.pc != 0x3399 {
                    cpu.instruction(&mut mem);
                }
                else { 
                    println!("Success"); 
                    done = true;
                    break;
                }
            }
            
        }
    }

    #[ignore]
    #[test]
    fn extended_opcodes_test() {

        let mut cpu = Mos6502::power(last_cycle, nmi);
        let mut mem = Mem { ram: load("65C02_extended_opcodes_test.bin") };
        cpu.r.pc = 0x0400;  // prog start address

        let mut done = false;
        while !done  {
            for _i in 0..10000 {
            println!("A:{:02x} X:{:02x} Y:{:02x} S:{:02x} {:}", cpu.r.a, cpu.r.x, cpu.r.y, cpu.r.s, disassemble(&mut mem, cpu.r.pc));
                if cpu.r.pc != 0x24a8 {
                    cpu.instruction(&mut mem);
                }
                else { 
                    println!("Success"); 
                    done = true;
                    break;
                }
            }
            
        }
    }
}
